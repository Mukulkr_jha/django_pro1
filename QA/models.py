from django.db import models

# Create your models here.

class QuestionModel(models.Model):
    """ This model is used to store the question statement
        and the type of the question i.e. whether it is MCQ
        or Integral .
    """
    stmt = models.TextField()
    QTYPE = (
        ('MCQ','MCQ'),
        ('ING','ING'),
    )
    type = models.CharField('Type of question ',max_length=5,choices=QTYPE)

    def __str__(self):
        return self.stmt+" | " +self.type



class AttemptModel(models.Model):
    """ This model will keep track of the score and if the
        user had attempted it or not
    """
    attempted = models.BooleanField()
    SCORE = (
        ('X','Not yet Answered!'),
        ('0','Wrong answer!'),
        ('1','Correct answer!'),

    )
    score = models.CharField(max_length=1,choices=SCORE)

    def __str__(self):
        return "Att = "+ str(self.attempted )+" score ="+self.score


class ChoiceModel(models.Model):
    """ This model is used to store the answer of the question
        and the choices of the question (if the type of the
        question is MCQ )
    """
    question = models.OneToOneField(
                        QuestionModel,
                        on_delete=models.CASCADE,
                        verbose_name='Associated Question',
                        )
    option1 = models.CharField(max_length=20,null=True, blank=True)
    option2 = models.CharField(max_length=20,null=True, blank=True)
    option3 = models.CharField(max_length=20,null=True, blank=True)
    option4 = models.CharField(max_length=20,null=True, blank=True)

    answer = models.IntegerField()

    att = models.OneToOneField(
                    AttemptModel,
                    on_delete = models.CASCADE,
                    verbose_name='Attempted or not',
    )

    def __str__(self):
        if self.option2 is None :
            return "ans is = "+str(self.answer)
        else:
            return self.option1+"..."+self.option2+"ans is "+str(self.answer)
