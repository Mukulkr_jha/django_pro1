from django.contrib import admin
from QA.models import QuestionModel,ChoiceModel,AttemptModel

# Register your models here.

admin.site.register(QuestionModel)
admin.site.register(AttemptModel)
admin.site.register(ChoiceModel)
