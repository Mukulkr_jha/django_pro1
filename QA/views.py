from django.shortcuts import render, redirect ,get_object_or_404
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect

from QA.models import QuestionModel,ChoiceModel


# Create your views here.

class IndexView(TemplateView):
    template_name='index.html'

def question_view(request,*args, **kwargs):
    pk =kwargs.get('pk')
    # print(pk)
    Q_obj = get_object_or_404(ChoiceModel, pk=pk)
    # print(Q_obj)
    context = {
        'Q_stmt':Q_obj.question.stmt,
        'Q_type':Q_obj.question.type,
        'Q_choice':Q_obj,
        'current_que': pk}   # setting the context variable for rendering
    # print(context)
    Attempt = Q_obj.att
    # print(Attempt)

    # if the method is post then we'll check the answer otherwise just display the questions

    if request.method=='POST':
        if request.POST['choice'] == str(Q_obj.answer) :
            print('your ans is correct!! with choice '+ request.POST['choice']+" and actual answer is "+str(Q_obj.answer))
            # setting the attempted and score field to be true

            Attempt.attempted = 'True'
            Attempt.score = '1'
            Attempt.save()

        else:                   # if the answer is incorrect
            print('Please try again!! with choice '+ request.POST['choice'] +" and actual answer is "+str(Q_obj.answer))
            # setting the attempted field to be true but the score remains zero (as answer guessed is incorrect )

            Attempt.attempted = 'True'
            Attempt.score = '0'
            Attempt.save()
        # nextpk =0
        nextpk = int(pk)+1   #  set the pk to next question
        total_ques = ChoiceModel.objects.all().count()

        if nextpk <= total_ques:
            return redirect('QA:test',pk=nextpk)
        return redirect('QA:score')

    else:    # condition when Get method is used
        return render(request,'question.html',context)


def ScoreView(request):

    # selecting the records which have their score attribute set to True

    attempts =ChoiceModel.objects.filter(att__attempted ='True').count()
    # print(attempts)
    score = ChoiceModel.objects.filter(att__score ='1').count()
    # print(score)
    return render(request,'score.html', {'score':score,'attempts':attempts })
