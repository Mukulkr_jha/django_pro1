from django.urls import path,include
from django.conf.urls import url

from QA.views import IndexView,ScoreView,question_view

app_name = 'QA'

urlpatterns = [
    url(r'^score/$',ScoreView,name='score'),
    #url(r'^ques/?P<pk>[0-9]+/',QuestionView,name='test'),
    path('<int:pk>/', question_view, name='test')
]
